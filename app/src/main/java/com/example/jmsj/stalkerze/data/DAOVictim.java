package com.example.jmsj.stalkerze.data;

import android.os.Environment;

import com.example.jmsj.stalkerze.model.Victim;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

public class DAOVictim extends DAOHelper<ArrayList<Victim>> {
    private static DAOVictim INSTANCE;
    private ArrayList<Victim> victims;
    private boolean sync;

    private DAOVictim() {
        super(new File(Environment.getExternalStorageDirectory(), "default"), "victim.obj");
        this.victims = new ArrayList<>();
        this.sync = false;
    }

    public static DAOVictim getINSTANCE(){
        if(INSTANCE==null) INSTANCE = new DAOVictim();
        return INSTANCE;
    }

    public void setFilePath(File path, String fileName) {
        super.setPath(path);
        super.setFileName(fileName);
    }

    public ArrayList<Victim> getVictims() throws FileNotFoundException, IOException,  ClassNotFoundException {
        if(!this.sync) {
            this.victims = super.loadObject();
            this.sync = true;
        }
        return this.victims;
    }

    public boolean addVictim(Victim v) throws FileNotFoundException, IOException, ClassNotFoundException {
        boolean firstTime = false;
        if(!this.sync){
            try{
                this.victims = super.loadObject();
            } catch (FileNotFoundException e){
                firstTime = true;
            }
            this.sync = true;
        }
        this.victims.add(v);
        super.saveObject(this.victims);
        return firstTime;
    }
}
