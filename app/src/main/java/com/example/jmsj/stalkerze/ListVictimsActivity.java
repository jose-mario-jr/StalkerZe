package com.example.jmsj.stalkerze;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.jmsj.stalkerze.model.Victim;
import com.example.jmsj.stalkerze.view.VictimsListAdapter;

public class ListVictimsActivity extends AppCompatActivity implements VictimsListAdapter.VictimListener {

    private RecyclerView recyclerViewVictimsList;
    private VictimsListAdapter victimsListAdapter;
    public static final String VICTIM_KEY = "com.example.jmsj.stalkerze.ListVictimsActivity.VICTIM";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_victims);

        recyclerViewVictimsList = (RecyclerView) findViewById(R.id.recyclerViewVictimsList);
        victimsListAdapter = new VictimsListAdapter(this);
        recyclerViewVictimsList.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewVictimsList.setHasFixedSize(true);
        recyclerViewVictimsList.setAdapter(victimsListAdapter);

    }

    @Override
    public void onClickVictimListener(Victim victim) {
        Intent intent = new Intent(getBaseContext(), ShowVictimActivity.class);
        intent.putExtra(VICTIM_KEY, victim);
        startActivity(intent);
    }
}
