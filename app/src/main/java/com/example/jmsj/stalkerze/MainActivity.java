package com.example.jmsj.stalkerze;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClickAddPerson(View view){
        Intent intent = new Intent(getBaseContext(), FormNewVictimActivity.class);
        startActivity(intent);
    }
    public void onClickListPeople(View view){
        Intent intent = new Intent(getBaseContext(), ListVictimsActivity.class);
        startActivity(intent);
    }
}
