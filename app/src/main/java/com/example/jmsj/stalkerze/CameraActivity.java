package com.example.jmsj.stalkerze;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

public class CameraActivity extends AppCompatActivity {

    private ImageButton btTakePic;
    private Button bt;
    private static int REQUEST_CAMERA_CODE = 3333;
    public static final String BITMAP_KEY = "com.example.jmsj.buddyfinderze.CameraActivity.BITMAP";
    private Bitmap bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);

        btTakePic = (ImageButton) findViewById(R.id.btTakePic);
        bt = (Button) findViewById(R.id.buttonSavePic);
    }

    public void onClickTakePic(View view){
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if(intent.resolveActivity(getPackageManager())!=null){
            startActivityForResult(intent, REQUEST_CAMERA_CODE);
        }
    }

    public void onClickSave(View view){
//        Bitmap bitmap = ((BitmapDrawable)btTakePic.getDrawable()).getBitmap();
//        DAOPics.getINSTANCE().addPic(bitmap);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(requestCode == REQUEST_CAMERA_CODE && resultCode == RESULT_OK && data != null){
            Bundle bundle = data.getExtras();
            if(bundle != null && bundle.containsKey("data")){
                bitmap = (Bitmap) bundle.get("data");

                Intent intent = new Intent();
                intent.putExtra(BITMAP_KEY, bitmap);
                setResult(RESULT_OK, intent);
//
//                btTakePic.setImageBitmap(bitmap);
//                bt.setVisibility(Button.VISIBLE);

                finish();
            }
        }
    }
}
